
clear all;
close all;

% Grab flow data from DB
mysql('closeall'); % close all open connections.

% Create a connection to the WDT db.
mysql( 'open', '10.8.0.1', 'root', '699.tmp' );
mysql('use', 'water_neil');

% Starting in August, compute features.
[flowrate_overall, ts] = mysql('select gallons1+gallons2+gallons3,unix_timestamp(timestamp) from flowrate where gallons1+gallons2+gallons3 < 50 and sensor_id = 154 and timestamp > "2016-01-24"');

% Compute cumulative flow instead of instantaneous flow
cumulative_flow = cumsum(flowrate_overall);

% Set up TS in hours since the beginning of experiment
%ts = (ts - ts(1)) / (60*15);

% Downsample to 15-minute time intervals
%flowrate_hour = splitapply(@sum, flowrate_overall, 1+floor(ts));

cumulative_flow = con2seq(cumulative_flow');

N = 100; % Number of time samples to use as NN input
M = 10000; % Training set size

nn = 1;
runtime = 0;
tsize = 0;

Xi = cumulative_flow(1:N); % NN Input
X = cumulative_flow(N+1:(M-1)); % Dataset to analyze
timex = ts(N+1:(end-1));

T = cumulative_flow(N+2:M);

net = newlind(X,T,Xi);

%view(net)

%%

testset = cumulative_flow(2*M+N+1:2*M+(M-1));

Y = net(testset,Xi);

figure;

plot(cell2mat(Y))
hold on;
plot(cell2mat(testset))

figure;
plot(abs(cell2mat(Y)-cell2mat(testset)))
ylabel 'Forecast Error'
xlabel 'Time'

